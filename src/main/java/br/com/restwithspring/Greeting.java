package br.com.restwithspring;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Greeting {

	private final long id;
	private final String content;

}
